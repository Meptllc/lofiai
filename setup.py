"""Module setup."""

import setuptools

PACKAGE_NAME = "lofiai"


with open("README.md", "r") as fh:
    long_description = fh.read()

if __name__ == "__main__":
    setuptools.setup(
        name=PACKAGE_NAME,
        version="0.0.1",
        author="Christopher Chin",
        author_email="ctchin13@gmail.com",
        packages=setuptools.find_packages(),
        python_requires=">=3.7.7",
        description="My project",
        long_description=long_description,
        long_description_content_type="text/markdown",

        install_requires=[
            'click',
            'spotdl',

            # TODO: Remove these development tools.
            'jedi',
            'pdbpp',
        ],
        entry_points={
            'console_scripts': [
                f'{PACKAGE_NAME}={PACKAGE_NAME}.cli:cli'
            ],
        },
    )
