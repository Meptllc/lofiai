var currentlyplaying = document.getElementById("currentlyplaying");
var audio = document.getElementById("audio");
var entries = document.getElementsByClassName("sample_entry");

function clearSelectedEntries() {
    for (var i = 0; i < entries.length; i++) {
        entries[i].classList.remove('selected_entry');
    }
}

// list_entry is a li.sample_entry element.
function setAudioSource(list_entry) {
    clearSelectedEntries();
    list_entry.classList.add('selected_entry');

    audio.pause();
    audio.currentTime = 0;
    audio.src = 'samples/' + list_entry.innerHTML + '.wav';
}

function entryClick(event) {
    // Find which sample is currently selected.
    var selected;
    for (var i = 0; i < entries.length; i++) {
        if (entries[i].classList.contains('selected_entry')) {
            selected = entries[i];
            break;
        }
    }
    if (selected == event.target) {
        if (audio.paused) {
            audio.play();
        } else {
            audio.pause();
        }
    } else {
        setAudioSource(event.target)
        audio.play();
    }
}

function main() {
    if (entries.length > 0) {
        setAudioSource(entries[0]);
    }

    for (var i = 0; i < entries.length; i++) {
        entries[i].addEventListener('click', entryClick);
    }
}

main();
