{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {
  name = "lofiai";
  buildInputs = with pkgs; [
  ];

  nativeBuildInputs = with pkgs; [
    jekyll
  ];
}
