# LoFiAi

# Gathering train/test data
Use your tool of choice to download 128bit 44.1kHz mp3 files. e.g. `spotdl`
`youtube-dl`

Example workflow

## Scraping Youtube Videos
Download audio only from Youtube
```
# Grab available formats if desired
youtube-dl -F https://www.youtube.com/watch?v=yPoBue-1nA4
# Download the audio only format.
youtube-dl -f 140 https://www.youtube.com/watch?v=yPoBue-1nA4
```

This package supplies a command for scraping redirect links from a Youtube video
description.
```
lofiai scrape 'https://www.youtube.com/watch?v=rA56B4JyTgI' | grep spotify | tee youtube-rA56B4JyTgI.txt
```

## Downloading the scraped urls
One can download a list of Spotify links like so. I was finding poor behaviour
with the default call to `spotdl` which explains the use of --search-format.
Likely spotdl searches by lyric metadata which is fairly useless for this genre.
```
spotdl --trim-silence --no-spaces -f '{artist}-{track-name}.{output-ext}' --search-format='{artist} - {track-name}' --list above-the-clouds.txt
```

I also modified my local spotdl to skip large files which would be indicative of
a lengthy video. Such as would happen if a single song video were unavailable
and instead we hit a compilation video. Something like the following into
command_line/core.py:download_track_from_metadata(). In an ideal world we would
compare the metadata of spotify with the length of the youtube video.
```
if stream['filesize'] > 11000000:
    logger.info('Skipping as it is larger than 11MB')
    return
```

## Other helpful commands
Print mp3 name with length, sorted.
```
for i in *.mp3; do echo -n $i ','; mp3info -p '%S\n' $i; done | sort -t, -k2 -V > somefile.txt
```
