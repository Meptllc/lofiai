import click
import os
import re
import requests
from bs4 import BeautifulSoup

from ..lib import AliasedGroup

@click.command()
@click.argument('url')
def desc_links(url):
    """Navigate all redirect links in a youtube description. Prints targets."""
    response = requests.get(url)
    soup = BeautifulSoup(response.text, 'html.parser')
    description_redirects = soup.select('a[href*=redirect]')
    for elem in description_redirects:
        response = requests.get('https://www.youtube.com' + elem.get('href'))
        soup = BeautifulSoup(response.text, 'html.parser')
        # Grab the link from the "Go to Site button"
        response = requests.get(soup.find(id='invalid-token-redirect-goto-site-button').get('href'))
        print(response.url)

@click.command()
@click.argument('path', type=click.Path(exists=True))
def dl_timestamp_names(path):
    """Downloads a list of song names in a given file using spotdl.

    The file format is a list of timestamps and song name pairings which is
    typical on Youtube descriptions and comments. e.g.

    ```

    0:00 idealism - Both of Us
    2:10 Keem the Cipher - [BLOSSOM.] (w sugi.wa)
    3:58 Jhfly - Crossings

    ```

    This will call something like `spotdl -s "idealism - Both of Us"`.
    This mostly exists because I don't want to write Bash to
    do this.
    """
    with open(path) as f:
        for line in f.readlines():
            line = line.strip()
            if not line:
                continue

            # Drop the timestamp.
            song_desc = ' '.join(line.split()[1:])
            os.system(spotdl_cmd(f'-s "{song_desc}"'))

@click.command()
@click.argument('path', type=click.Path(exists=True))
def dl_list(path):
    os.system(spotdl_cmd(f'--list "{path}"'))


def spotdl_cmd(args):
    return 'yes N | spotdl --trim-silence --no-spaces -f {artist}-{track-name}.{output-ext} --search-format="{artist} - {track-name}" ' + args


@click.group(cls=AliasedGroup)
def scrape():
    pass

@scrape.group(cls=AliasedGroup)
def yt():
    """Youtube tools."""
    pass

yt.add_command(desc_links)
yt.add_command(dl_timestamp_names)
yt.add_command(dl_list)
